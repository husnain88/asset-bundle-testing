﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadBundles : MonoBehaviour {

	public string url;


	// Use this for initialization
	void Start () {

		WWW source = WWW.LoadFromCacheOrDownload (url,0);

		StartCoroutine (Download (source));

	}

	IEnumerator Download(WWW urlll)
	{
		yield return urlll;

		AssetBundle ab = urlll.assetBundle;

		if (urlll.error == null) {
			GameObject laodedAsset = ab.LoadAsset ("cube") as GameObject;

			Instantiate (laodedAsset);
		} else {
			Debug.LogError (urlll.error);
		}
	}
}
